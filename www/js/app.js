angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('read', {
      url: '/read',
      templateUrl: 'templates/read.html',
      controller: 'ReadCtrl'
    })

    .state('create', {
      url: '/create',
      templateUrl: 'templates/create.html',
      controller: 'CreateCtrl'
    })

    .state('update', {
      url: '/update/:id',
      templateUrl: 'templates/update.html',
      controller: 'UpdateCtrl'
    });

    $urlRouterProvider.otherwise('/read');
})

.controller('ReadCtrl', function($scope, $state, ContatoService) {

  // $scope.contatos = contatos;
  $scope.contatos = ContatoService.read();

  $scope.create = function() {
    $state.go('create');
  }

  $scope.update = function(id) {
    $state.go('update', {id: id});
  }

  $scope.delete = function(id) {
    // contatos.splice(id, 1);
    ContatoService.delete(id);
  }
})

.controller('CreateCtrl', function($scope, $state, ContatoService) {

  $scope.contato = {};

  $scope.save = function(contato) {
    // contatos.push(contato);
    ContatoService.create(contato);
    $state.go('read');
  }

})

.controller('UpdateCtrl', function($scope, $state, $stateParams, ContatoService) {

  var id = $stateParams.id;

  // $scope.contato = angular.copy(contatos[id]);
  $scope.contato = ContatoService.readOne(id);

  $scope.save = function(contato) {
    // contatos[id] = contato;
    ContatoService.update(id, contato);
    $state.go('read');
  }
})

.factory('ContatoService', function() {

  // var contatos = [
  //   {
  //     nome: 'Fulano',
  //     email: 'fulano@email.com'
  //   },
  //   {
  //     nome: 'Beltrano',
  //     email: 'beltrano@email.com'
  //   },
  //   {
  //     nome: 'Ciclano',
  //     email: 'ciclano@email.com'
  //   }
  // ];

  var contatos = JSON.parse(window.localStorage.getItem('database') || '[]');

  function persistir() {
    window.localStorage.setItem('database', JSON.stringify(contatos));
  }

  return {

    read: function() {
      return contatos;
    },

    readOne: function(id) {
      return angular.copy(contatos[id]);
    },

    create: function(contato) {
      contatos.push(contato);
      persistir();
    },

    update: function(id, contato) {
      contatos[id] = contato;
      persistir();
    },

    delete: function(id) {
      contatos.splice(id, 1);
      persistir();
    }

  }

})

