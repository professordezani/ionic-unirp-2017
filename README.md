# Desenvolvimento de Aplicativos Híbridos com Ionic #

Worksho sobre o desenvolvimento de aplicativos híbridos utilizando Ionic apresentado na Semana da Computação da UNIRP, 2017.

## Prof. Dr. Henrique Dezani ##
* Bacharel em Ciência da Computação, mestre e doutor em Engenharia Elétrica (UNESP e UNICAMP) e pós-doutorando em Jogos Digitais pela USP.
* Leciona na FATEC Rio Preto disciplinas nas área de Programação O.O, Web e Mobile, desde 2006, para cursos de graduação e pós-graduação.
* Atua como desenvolvedor Web e Mobile (MVP)
* Consultor e Palestrante nas área de aplicativos híbridos e gamificação.

### Objetivo ###
Tem-se por objetivo neste curso desenvolver um aplicativo Ionic (v1) capaz de realizar o armazenamento persistente dos dados.

### Sumário ###

* CLI (Comando Line Interface) e a Criação do projeto
* Criação do serviço de armazenamento persistente em JSON
* Criação das Rotas e Telas do aplicativo
* Criação dos controladores das telas
* Criação do serviço de manipulação dos dados

### Links Úteis ###

* [Prof. Dr. Henrique Dezani](http://www.dezani.com.br)
* [Curso Completo de Ionic na Udemy](https://www.udemy.com/crie-aplicativos-moveis-com-html-css-e-javascript/learn)
* [IonicView](http://view.ionic.io/)

### CLI ###

* Criando um projeto `ionic start AgendaApp blank --v1`
* Executando o projeto no navegador `ionic start -l`
* Executando o aplicativo no dispositivo `ionic upload`

### Código do Projeto no Ionic View ###
48ab04c0